var bookApp = angular.module('bookcar.services', []);
bookApp.factory('LoginService',function($q, $http) {
        return {
            Login: function(dataJSON) {
                var promise = $http({
                    url: USER_LOGIN,
                    method: 'POST',
                    data:   dataJSON,
                    headers: GLOBAL_HEADERS
                }).success(function(data, status, headers, config) {
                    return data;
                });
                return promise;
            }
            
        }
        
    }).factory('RegisterService',function($q, $http) {
        return {
            RegisterPromotion: function(dataJSON) {
                var promise = $http({
                    url: REGISTER_PROMOTION,
                    method: 'POST',
                    data:   dataJSON,
                    headers: GLOBAL_HEADERS
                }).success(function(data, status, headers, config) {
                    return data;
                });
                return promise;
            }
         }
        
    }).factory('CategoryService',function($q, $http) {
        return {
            CategoryList: function(dataJSON) {
                var promise = $http({
                    url: CATEGORY_LISTING,
                    method: 'POST',
                    data:   dataJSON,
                    headers: GLOBAL_HEADERS
                }).success(function(data, status, headers, config) {
                    return data;
                });
                return promise;
            },
            
            ServiceList: function(dataJSON) {
                var promise = $http({
                    url: SERVICE_LISTING,
                    method: 'POST',
                    data:   dataJSON,
                    headers: GLOBAL_HEADERS
                }).success(function(data, status, headers, config) {
                    return data;
                });
                return promise;
            },
             ServiceDetail: function(dataJSON) {
                var promise = $http({
                    url: SERVICE_DETAIL,
                    method: 'POST',
                    data:   dataJSON,
                    headers: GLOBAL_HEADERS
                }).success(function(data, status, headers, config) {
                    return data;
                });
                return promise;
            },  
             LockCounter: function(dataJSON) {
                var promise = $http({
                    url: LOCK_COUNTER,
                    method: 'POST',
                    data:   dataJSON,
                    headers: GLOBAL_HEADERS
                }).success(function(data, status, headers, config) {
                    return data;
                });
                return promise;
            },
             UnlockCounter: function(dataJSON) {
                var promise = $http({
                    url: UNLOCK_COUNTER,
                    method: 'POST',
                    data:   dataJSON,
                    headers: GLOBAL_HEADERS
                }).success(function(data, status, headers, config) {
                    return data;
                });
                return promise;
            }
            
        }
        
    }).factory('BookService',function($q, $http) {
        return {
            BookAppointment: function(dataJSON) {
                var promise = $http({
                    url: BOOK_APPOINTMENT,
                    method: 'POST',
                    data:   dataJSON,
                    headers: GLOBAL_HEADERS
                }).success(function(data, status, headers, config) {
                    return data;
                });
                return promise;
            },
            GenerateToken: function(dataJSON) {
                var promise = $http({
                    url: GENERATE_TOKEN,
                    method: 'POST',
                    data:   dataJSON,
                    headers: GLOBAL_HEADERS
                }).success(function(data, status, headers, config) {
                    return data;
                });
                return promise;
            }
         }
        
    })
  
    