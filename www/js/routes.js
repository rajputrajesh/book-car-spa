angular.module('bookcar.routes', [])
.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
  $ionicConfigProvider.navBar.alignTitle('center');

  var checkRememberMe = function($q, $state, $ionicViewService, $ionicLoading, $timeout, $location){

    var deferred = $q.defer();
      if (localStorage.getItem("userData") === null) {
        $timeout(deferred.resolve, 0);
      }else{
        $timeout(deferred.resolve, 0);
        $ionicViewService.nextViewOptions({disableBack: true});
        $location.path('/sign-in');
      }
     return deferred.promise;
   }

  $stateProvider
   .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })
   .state('landing', {
      cache:false,
      url: '/landing-home',
      templateUrl: 'templates/landing-home.html',
      controller: 'SignInCtrl',
      resolve:{checked:checkRememberMe}
    })
   .state('signin', {
      cache:false,
      url: '/sign-in',
      templateUrl: 'templates/sign-in.html',
      controller: 'SignInCtrl'
    })
   
   .state('app.register', {
      cache:false,
      url: '/register',
      views: {
        'menuContent': {
          templateUrl: 'templates/register.html',
          controller: 'RegisterCtrl'
         }
      }
  })
   .state('app.token', {
      cache:false,
      url: '/token',
      views: {
        'menuContent': {
          templateUrl: 'templates/token.html',
          controller: 'TokenCtrl'
         }
      }
  })
  .state('app.booked', {
      cache:false,
      url: '/booked',
      views: {
        'menuContent': {
          templateUrl: 'templates/booked.html',
          controller: 'BookedOnlineCtrl'
         }
      }
  })
   .state('app.categories', {
      cache:false,
      url: '/categories',
      views: {
        'menuContent': {
          templateUrl: 'templates/categories.html',
          controller: 'CategoryCtrl'
        }
      }
  })
   .state('app.services', {
      cache:false,
      url: '/services/:centerServiceId',
      views: {
        'menuContent': {
          templateUrl: 'templates/services.html',
          controller: 'ServiceCtrl'
        }
      }
  })
    .state('app.pricelist', {
      url: '/pricelist/:centerServiceId',
      cache:false,
      views: {
        'menuContent': {
          templateUrl: 'templates/pricelist.html',
          controller: 'PriceListCtrl'
        }
      }
  })
    .state('app.carnumber', {
      url: '/carnumber/:centerServiceId',
      cache:false,
      views: {
        'menuContent': {
          templateUrl: 'templates/carnumber.html',
          controller: 'CarNumberCtrl'
        }
      }
  })
    .state('app.servicedetail', {
      url: '/servicedetail/:centerServiceId',
      cache:false,
      views: {
        'menuContent': {
          templateUrl: 'templates/servicedetail.html',
          controller: 'ServiceDetailCtrl'
        }
      }
  })
       .state('app.thankyou', {
      cache:false,
      url: '/thankyou',
      views: {
        'menuContent': {
          templateUrl: 'templates/thankyou.html',
          controller: 'TnxCtrl'
        }
      }
  })
    .state('app.registerthanks', {
      url: '/registerthanks/:newtoken',
      cache:false,
      views: {
        'menuContent': {
          templateUrl: 'templates/registerthanks.html',
          controller: 'RegisterTnxCtrl'
        }
      }
  })
  .state('app.registerthank', {
     url: '/registerthank/',
      cache:false,
      views: {
        'menuContent': {
          templateUrl: 'templates/registerthank.html'
        }
      }
  })
  $urlRouterProvider.otherwise('/landing-home');
});
