angular.module('bookcar.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $ionicHistory, $state, CategoryService, $ionicPopup, $ionicLoading, $location) {
    //$scope.portImage="http://";
    $scope.gotoTokenScreen = function() {
      if ($state.current.name == "app.servicedetail") {
        var unlockCounterDetails = JSON.parse(window.localStorage['UNLOCK_COUNTER_DATA']);
        CategoryService.UnlockCounter(unlockCounterDetails).success(function(data) {
          console.log(data);
          if (data.status == 200) {
            $state.go('app.token');
          }
        });
      } else {
        $state.go('app.token');
      }
    }

    $scope.logout = function() {
      $scope.data = {};
      $scope.data.pin = '';
      var myPopup = $ionicPopup.show({
        templateUrl: 'templates/pin.html',
        title: 'Enter Your Password ',
        scope: $scope,
        buttons: [{
          text: '<b>Cancel</b>'
        }, {
          text: '<b>ok</b>',
          type: 'custom-button',
          onTap: function(e) {
            if (!$scope.data.pin) {
              var alertPopup = $ionicPopup.alert({
                title: 'Invalid Password!',
                template: 'Please Enter A Valid Password!',
                buttons: [{
                  text: '<b>Ok</b>',
                  type: 'pink-white-theme-color'
                }]
              });
              e.preventDefault();
            } else {

              if (localStorage.getItem("user_password") != null) {
                $scope.user_password = JSON.parse(window.localStorage['user_password']);
              }
              if ($scope.user_password == $scope.data.pin) {

                localStorage.clear();
                $state.go('signin');

              } else {
                var alertPopup = $ionicPopup.alert({
                  title: 'Invalid Password!',
                  template: 'Please check your credentials!',
                  buttons: [{
                    text: '<b>Ok</b>',
                    type: 'pink-white-theme-color'
                  }]
                });
                //e.preventDefault();


              }


            }
          }
        }]
      });
      myPopup.then(function(res) {
        console.log('Tapped!', res);
      });
    };

    $scope.$on('$ionicView.loaded', function(e) {

      console.log($ionicHistory.currentView().stateName);
      if ($state.current.name == "app.token") {
        $scope.tokenStatus = false;
        //Maybe grab some user info and stuff it in a variable.
      } else {
        $scope.tokenStatus = true;
      }

    });



  })
  .controller('SignInCtrl', function($scope, $state, LoginService, $ionicLoading, $ionicPopup) {
    $scope.types = [{
      key: "1",
      value: "Guest"
    }, {
      key: "0",
      value: "Vendor"
    }];
    $scope.signIn = function(user) {
      $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0,
        correctOrientation: true
      });
      var userData = {};

      userData.email = user.username;
      userData.password = user.password;
      LoginService.Login(userData).success(
        function(data) {
          $ionicLoading.hide();
          if (data.status == 200) {
            window.localStorage['userData'] = JSON.stringify(data);
            window.localStorage['user_password'] = JSON.stringify(user.password);
            var userData = JSON.parse(window.localStorage['userData'] || '{}');
            $state.go('app.token');
          } else {
            var alertPopup = $ionicPopup.alert({
              title: 'Login failed!',
              template: 'Please check your credentials!',
              buttons: [{
                text: '<b>Ok</b>',
                type: 'pink-white-theme-color'
              }]
            });
          }
        });
    }

  })

.controller('TokenCtrl', function($scope, $state, LoginService, $ionicLoading, $ionicPopup) {
  $scope.issueToken = true;
  $scope.tookenIn = function() {
    $scope.bookToken = false;
  };

  $scope.bookendIn = function() {
    $scope.issueToken = false;
  };

})



.controller('RegisterCtrl', function($scope, $state, $ionicLoading, RegisterService, $ionicPopup) {
  $ionicLoading.hide();
  //$scope.regex = '/^[A-Za-z]\w{7,14}$/';
  $scope.mobregex = '\\d+';
  $scope.nameregex = '[A-Za-z\\s]*';
  if (localStorage.getItem("userData") != null) {
    $scope.userData = JSON.parse(window.localStorage['userData']);
  }
  if (localStorage.getItem('carData') != null) {
    $scope.carData = JSON.parse(localStorage.getItem('carData'));
  }
  $scope.register = function(user) {
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0,
      correctOrientation: true
    });
    user.center_id = $scope.userData.data.id;
    user.car_number = $scope.carData.carnumber;
    RegisterService.RegisterPromotion(user).success(
      function(data) {
        console.log(data);
        $ionicLoading.hide();
        if (data.status == 200) {
          //        var alertPopup = $ionicPopup.alert({
          //     title:    'Registered Successfully!',
          //     template: 'Your credentials has been mailed. You may log in now.',
          //     buttons:[
          //       {
          //         text: '<b>Ok</b>',
          //         type: 'pink-white-theme-color'
          //       }
          //     ]
          // });
          $state.go('app.registerthank');
        } else {
          var alertPopup = $ionicPopup.alert({
            title: 'Registration failed!',
            template: 'Oop\'s! something went wrong.',
            buttons: [{
              text: '<b>Ok</b>',
              type: 'pink-white-theme-color'
            }]
          });
        }

      });
    // $state.go('app.registerthanks');
  }

  $scope.showConfirm = function() {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Cancel',
      template: 'Are you sure you want to cancel?'
    });

    confirmPopup.then(function(res) {
      if (res) {
        $state.go('app.token')
      } else {
        console.log('Deletion canceled !');
      }
    });
  };
})


.controller('CategoryCtrl', function($scope, $state, $ionicLoading, CategoryService, $location) {
  if (localStorage.getItem("userData") != null) {
    $scope.userData = JSON.parse(window.localStorage['userData']);
  }
  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });
  var catData = {};
  catData.type = "guest";
  catData.center_id = $scope.userData.data.id;

  console.log(catData);

  CategoryService.CategoryList(catData).success(function(categorydata) {
    console.log(JSON.stringify(categorydata));
    $ionicLoading.hide();
    if (categorydata.status == 200) {
      var categoryList = categorydata.data;
      var products_page = [];
      var products_pages = [];
      //categoryList.length=3;
      var count = 3;
      $scope.products_pages = products_pages;
      for (var i = 0; i < categoryList.length; i++) {
        if (count > 1) {
          products_page.push(categoryList[i]);
          count--;
          if (categoryList.length === i + 1) {
            products_pages.push(products_page);
            return;
          }
        } else {
          products_page.push(categoryList[i]);
          products_pages.push(products_page);
          products_page = [];
          count = 3;
        }
      }
    }
  });
  $scope.getServices = function(centerServiceId, categoryName) {
    window.localStorage['categoryName'] = JSON.stringify(categoryName);
    $location.path("app/services/" + centerServiceId);
  };
})


.controller('ServiceCtrl', function($scope, $state, $ionicLoading, $stateParams, CategoryService, $location) {
  if (localStorage.getItem("userData") != null) {
    $scope.userData = JSON.parse(window.localStorage['userData']);
  }
  if (localStorage.getItem("userData") != null) {
    $scope.categoryName = JSON.parse(window.localStorage['categoryName']);
  }
  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });
  var serviceData = {};
  serviceData.type = "guest";
  serviceData.center_id = $scope.userData.data.id;
  serviceData.center_service_id = $stateParams.centerServiceId;
  CategoryService.ServiceList(serviceData).success(function(servicedata) {
    console.log(JSON.stringify(servicedata));
    $ionicLoading.hide();
    if (servicedata.status == 200) {
      var serviceLists = servicedata.data;

      $scope.counting = 0;
      var mod = serviceLists.length % 3;


      $scope.counting = serviceLists.length - mod;

      var service_list = [];
      var service_lists = [];
      //var count = $scope.counting;
      var count = 3;

      //console.log(serviceLists.length)

      // console.log(count)



      $scope.service_lists = service_lists;


      for (var i = 0; i < serviceLists.length; i++) {
        if (count > 1) {
          service_list.push(serviceLists[i]);
          count--;
          if (serviceLists.length === i + 1) {
            service_lists.push(service_list);
            return;
          }
        } else {
          service_list.push(serviceLists[i]);
          service_lists.push(service_list);
          service_list = [];
          count = 3;
        }
      }

    }
  });
  $scope.getPriceList = function(categoryId, serviceImage, serviceName) {
    window.localStorage['serviceImage'] = JSON.stringify(serviceImage);
    window.localStorage['serviceName'] = JSON.stringify(serviceName);
    $location.path("app/pricelist/" + categoryId);
  }
})
.controller('CarNumberCtrl', function($scope, $state, $ionicLoading, $location, $stateParams) {
  var JsonCar = localStorage.getItem('carData');
  JsonCar = JSON.parse(JsonCar);
  console.log(JsonCar);

  $scope.getServiceDetail = function(car) {
    JsonCar.carnumber = car.carnumber;
    localStorage.setItem('carData', JSON.stringify(JsonCar));
    $location.path("app/servicedetail/" + $stateParams.centerServiceId);
  }

})

.controller('PriceListCtrl', function($scope, $state, $ionicLoading, $location, $stateParams, CategoryService) {
  $scope.nameregex = '[0-9A-Za-z\\s]*';
  if (localStorage.getItem("userData") != null) {
    $scope.userData = JSON.parse(window.localStorage['userData']);
  }
  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });
  var serviceDetails = {};
  serviceDetails.center_id = $scope.userData.data.id;
  serviceDetails.center_service_id = $stateParams.centerServiceId;
  CategoryService.ServiceDetail(serviceDetails).success(function(serviceDetaildata) {
    console.log("HERE in DATA --- ",   JSON.stringify(serviceDetaildata));
    $ionicLoading.hide();
    if (serviceDetaildata.status == 200) {

      var priceLists = serviceDetaildata.data.Services.ServicePricingOption;
      console.log(priceLists)

      var price_list = [];
      var price_lists = [];
      $scope.counting = 0;
      var mod = priceLists.length % 1;
      $scope.counting = priceLists.length - mod;
      var count = $scope.counting;
      $scope.price_lists = price_lists;

      console.log(priceLists)
      if (priceLists[0].sell_price != null) {
        $scope.actual_price = priceLists[0].sell_price;
      }else{
        $scope.actual_price = priceLists[0].full_price;
      }

      //console.log(price)
      if (priceLists.length > 0) {
        $scope.car = {
          priceData: priceLists[0].eng_name + "+" + priceLists[0].duration + "+" + $scope.actual_price + "+" + priceLists[0].id
        }
        console.log($scope.car)
      }
      for (var i = 0; i < priceLists.length; i++) {
        if (count > 1) {
          price_list.push(priceLists[i]);
          count--;
          if (priceLists.length === i + 1) {
            price_lists.push(price_list);
            return;
          }
        } else {
          price_list.push(priceLists[i]);
          price_lists.push(price_list);
          price_list = [];
          count = 1;
        }
      }
    }
  });
  $scope.getServiceDetail = function(car) {

    console.log(JSON.stringify(car));
    localStorage.setItem('carData', JSON.stringify(car));
    $location.path("app/carnumber/" + $stateParams.centerServiceId);
    //$location.path("app/servicedetail/" + $stateParams.centerServiceId);
    //$state.go('app.servicedetail');

  }
})


.controller('ServiceDetailCtrl', function($scope, $state, $ionicLoading, CategoryService, BookService, $stateParams) {
  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });
  console.log($stateParams.centerServiceId);
  if (localStorage.getItem("serviceImage") != null) {
    $scope.serviceImage = JSON.parse(window.localStorage['serviceImage']);
    //console.log(JSON.parse(window.localStorage['serviceImage']));
  }
  if (localStorage.getItem("serviceImage") != null) {
    $scope.serviceName = JSON.parse(window.localStorage['serviceName']);
    //console.log(JSON.parse(window.localStorage['serviceName']));
  }
  if (localStorage.getItem('carData') != null) {
    $scope.carData = JSON.parse(localStorage.getItem('carData'));
    var arrDetail = $scope.carData['priceData'].split('+');

    //console.log(JSON.parse(window.localStorage['carData']));
  }
  if (localStorage.getItem("userData") != null) {
    $scope.userData = JSON.parse(window.localStorage['userData']);
    //console.log(JSON.parse(window.localStorage['userData']));
  }
  if ($scope.carData.priceData) {
    $scope.carDetail = $scope.carData.priceData.split('+');
  }

  var counterDetail = {};
  var unlockCounterDetails = {};

  counterDetail.center_id = $scope.userData.data.id;
  counterDetail.center_service_id = $stateParams.centerServiceId;
  counterDetail.service_pricing_option_id = arrDetail[3];
  console.log("HERE ------>", counterDetail);

  unlockCounterDetails.center_id = $scope.userData.data.id;
  unlockCounterDetails.center_service_id = $stateParams.centerServiceId;

  CategoryService.LockCounter(counterDetail).success(
    function(lockDetaildata) {
      console.log("RESPONSE ------>", lockDetaildata);
      $ionicLoading.hide();
      if (lockDetaildata.status == 200) {
        unlockCounterDetails.counter_id = lockDetaildata.data.id;
        unlockCounterDetails.service_date = lockDetaildata.data.date;
        unlockCounterDetails.service_time = lockDetaildata.data.time;
        $scope.counter_name = lockDetaildata.data.counter_name;
        $scope.counter_time = lockDetaildata.data.time;
        if ($scope.counter_time) {
          $scope.counterTiming = $scope.counter_time.split(':');
          $scope.Timing = {};
          $scope.Timing.hour = $scope.counterTiming[0];
          $scope.secondTiming = $scope.counterTiming[1].split(' ');
          $scope.Timing.minute = $scope.secondTiming[0];
          $scope.Timing.ampm = $scope.secondTiming[1];
          console.log($scope.Timing);
        }

        $scope.counter_date = lockDetaildata.data.date;
        if ($scope.counter_date) {
          $scope.counterDate = $scope.counter_date.split('-');
          var month = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
          var day = new Array("Monday", "Tuesday", "Webnesday", "Thrusday", "Friday", "Saturday", "Sunday");
          var counterDateFormat = new Date($scope.counterDate[2], $scope.counterDate[1] - 1, $scope.counterDate[0]);
          $scope.counterPrintDate = counterDateFormat.getDate() + " " + month[counterDateFormat.getMonth()] + ", " + counterDateFormat.getFullYear() + " " + day[counterDateFormat.getDay()];
        }
        $scope.center_counter_id = lockDetaildata.data.id;
        console.log(lockDetaildata);
        window.localStorage['UNLOCK_COUNTER_DATA'] = JSON.stringify(unlockCounterDetails);
      }
    });

  $scope.book = function() {
    // console.log($scope.carDetail)
    var bookingData = {};
    bookingData.user_id = 0;
    bookingData.center_id = $scope.userData.data.id;
    bookingData.center_counter_id = $scope.center_counter_id
    bookingData.center_service_id = $stateParams.centerServiceId
    bookingData.service_pricing_options_id = $scope.carDetail[3]
    bookingData.appointment_price = $scope.carDetail[1]
    bookingData.appointment_duration = $scope.carDetail[2]
    bookingData.car_number = $scope.carData.carnumber;
    bookingData.appointment_start_date = $scope.counter_date + " " + $scope.counter_time;

    //console.log(bookingData)
    BookService.BookAppointment(bookingData).success(
      function(bookDetaildata) {
        $ionicLoading.hide();
        if (bookDetaildata.status == 200) {
          bookingData.tokenNo = bookDetaildata.token;
          window.localStorage['bookingData'] = JSON.stringify(bookingData);
          $state.go('app.thankyou');
        }
      });
  }

  $scope.unlockCounter = function() {
    console.log(unlockCounterDetails);
    CategoryService.UnlockCounter(unlockCounterDetails).success(function(data) {
      console.log(data);
      if (data.status == 200) {
        $state.go('app.token');
      }
    });
  }

})

.controller('TnxCtrl', function($scope, $state, $ionicLoading, $timeout) {
  if (localStorage.getItem("bookingData") != null) {
    $scope.bookingData = JSON.parse(window.localStorage['bookingData']);
    $scope.tokenNumber = $scope.bookingData.tokenNo.toString().split('');
    // console.log($scope.t)
    //$scope.bookingData.token.split('')
  }

})



.controller('BookedOnlineCtrl', function($scope, $state, $ionicLoading, $location, BookService, $ionicPopup) {

  var token = {};
  $scope.createToken = function(identificationNumber) {
    //console.log(identificationNumber)
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    token.book_reference_no = identificationNumber.identification;

    BookService.GenerateToken(token).success(
      function(data) {
        //console.log(lockDetaildata)
        $ionicLoading.hide();
        if (data.status == 200) {
          //data={status: "200", token: "6722", message: "Your book token number is- 6722"}

          //$state.go('app.registerthanks');
          $location.path("app/registerthanks/" + data.token);
        } else {

          var alertPopup = $ionicPopup.alert({
            title: 'Invalid!',
            template: 'Booking Id is invalid.',
            buttons: [{
              text: '<b>Ok</b>',
              type: 'pink-white-theme-color'
            }]
          });
        }
      });
  }
})



.controller('RegisterTnxCtrl', function($scope, $state, $ionicLoading, $stateParams, $timeout) {
  $scope.tokenNo = $stateParams.newtoken;;
  $scope.tokenNumber = $scope.tokenNo.toString().split('');
})
  .directive('myRepeatDirective', function($state, CategoryService) {

    return function(scope, element, attrs) {

      var seconds1 = (30000 / 1000).toFixed(1);

      console.log(seconds1);
      if (seconds1 < 0) {
        scope.countDown = "Expired";
        scope.minutes = 0
        scope.seconds = 0
        console.log("Expired", scope.countDown)
      } else {
        var tenMinutes = seconds1;
        var timer = tenMinutes,
          minutes, seconds;
        var Interval = setInterval(function() {
          countDownTimer();
        }, 1000);
      }

      function countDownTimer() {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        scope.countDown = minutes + ":" + seconds;

        scope.minutes = minutes
        scope.seconds = seconds

        if (scope.countDown == '00' + ":" + '00') {

          clearInterval(Interval);
          $state.go($state.current, {}, {
            reload: true
          });
          scope.countDown = "Expired";
          scope.minutes = 0
          scope.seconds = 0
          if($state.current.name == "app.servicedetail"){
            $state.go('app.token');
          }
          //console.log("Expired",scope.countDown,scope.minutes,scope.seconds)

        }
        if (--timer < 0) {
          timer = tenMinutes;
        }
        scope.$apply();
      }
    }
  })

.directive('myRepeatDirectiveee', function($state, CategoryService) {
    return function(scope, element, attrs) {
      var seconds1 = (30000 / 1000).toFixed(1);
      if (seconds1 < 0) {
        scope.countDown = "Expired";
        scope.minutes = 0
        scope.seconds = 0
        console.log("Expired", scope.countDown)
      } else {
        var tenMinutes = seconds1;
        var timer = tenMinutes,
          minutes, seconds;
        var Interval = setInterval(function() {
          countDownTimer();
        }, 1000);
      }

      function countDownTimer() {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        scope.countDown = minutes + ":" + seconds;

        scope.minutes = minutes
        scope.seconds = seconds

        if (scope.countDown == '00' + ":" + '00') {

          clearInterval(Interval);
          $state.go($state.current, {}, {
            reload: true
          });
          //scope.countDown = "Expired";
          scope.minutes = 0
          scope.seconds = 0

          if($state.current.name == "app.registerthanks"){
            $state.go('app.token');
          }
        }
        if (--timer < 0) {
          timer = tenMinutes;
        }
        scope.$apply();
      }
    }
  })
  .directive('focusMe', function($timeout) {
    return {
      link: function(scope, element, attrs) {
        if (attrs.focusMeDisable === "true") {
          return;
        }
        $timeout(function() {
          element[0].focus();
          if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            console.log("rntered in focus me ");
            cordova.plugins.Keyboard.show(); //open keyboard manually
          }
        }, 350);
      }
    };
  });